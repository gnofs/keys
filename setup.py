import sqlite3
import re
import logging
DATABASE="keys.sqlite"

def initial_setup(args):
    commands = """
CREATE TABLE IF NOT EXISTS progress(text TEXT, linecount INT, PRIMARY KEY (text));
CREATE TABLE IF NOT EXISTS stat (character CHAR, time FLOAT, correct INT, errors INT, session DATETIME);
CREATE TABLE IF NOT EXISTS syllable (syllable TEXT, type TEXT, language TEXT);
CREATE TABLE IF NOT EXISTS wordlength (num_syllables INT, probability FLOAT, language TEXT);
DROP VIEW IF EXISTS accumulated;
CREATE VIEW accumulated AS SELECT character, sum(time) AS time, sum(correct) AS correct,  sum(errors) AS errors FROM stat GROUP BY character HAVING session IN (SELECT session from stat LIMIT 10);
DROP VIEW IF EXISTS recent_sessions;
CREATE VIEW recent_sessions AS SELECT DISTINCT session FROM stat ORDER BY session DESC LIMIT 10;
DROP VIEW IF EXISTS recent_stat;
CREATE VIEW recent_stat AS SELECT * FROM stat WHERE session in (SELECT * FROM recent_sessions);
DROP VIEW IF EXISTS worst_wpm;
CREATE VIEW worst_wpm AS SELECT character, 60/(sum(time)/sum(correct))/5 AS wpm, 1.0*sum(errors)/sum(correct) as error_rate FROM recent_stat GROUP BY character HAVING sum(correct)>0 ORDER BY wpm;
DROP VIEW IF EXISTS worst_error_rate;
CREATE VIEW worst_error_rate AS SELECT character, 60/(sum(time)/sum(correct))/5 AS wpm, 1.0*sum(errors)/sum(correct) as error_rate FROM recent_stat GROUP BY character HAVING sum(correct)>0 AND sum(errors)>0 ORDER BY error_rate DESC;
DROP VIEW IF EXISTS wpm_per_date;
CREATE VIEW wpm_per_date AS SELECT strftime("%Y-%m-%d %H:%M",session) AS period, round(60/(sum(time)/sum(correct))/5,2) as wpm FROM recent_stat GROUP BY period ORDER BY period;
DROP VIEW IF EXISTS wpm_per_session;
CREATE VIEW wpm_per_session AS SELECT session, round(60/(sum(time)/sum(correct))/5,2) as wpm FROM recent_stat GROUP BY session ORDER BY session;
"""
    with sqlite3.connect(DATABASE) as conn:
        for command in commands.split('\n'):
            conn.execute(command)



def import_words(word_file, language):
    commands = """
CREATE TABLE IF NOT EXISTS syllable (syllable TEXT, type TEXT, language TEXT);
CREATE TABLE IF NOT EXISTS wordlength (num_syllables INT, probability FLOAT, language TEXT);
DROP VIEW IF EXISTS accumulated;
"""
    with sqlite3.connect(DATABASE) as conn:
        for command in commands.split('\n'):
            conn.execute(command)

    syllables = get_syllables(word_file)

    probabilities = dict()
    for num in range(1,10):
        probabilities[num] = len([x for x in syllables if len(x) == num])

    prefixes = [a[0] for a in syllables if a[0]]
    infixes = [y for x in [a[1:-1] for a in syllables if a[1:-1]] for y in x]
    suffixes = [a[-1] for a in syllables if a[-1]]

    with sqlite3.connect(DATABASE) as conn:
        conn.execute('DELETE FROM wordlength WHERE language=?', (language,))
        conn.executemany('INSERT INTO wordlength VALUES (?,?,?)', [(x, y/sum(probabilities.values()), language) for (x, y) in probabilities.items()])
        conn.execute('DELETE FROM syllable WHERE language=?', (language,))
        conn.executemany('INSERT INTO syllable VALUES (?,"prefix", ?)', [(p,language) for p in prefixes])
        conn.executemany('INSERT INTO syllable VALUES (?,"infix", ?)', [(p,language) for p in infixes])
        conn.executemany('INSERT INTO syllable VALUES (?,"suffix", ?)', [(p,language) for p in suffixes])


def get_syllables(wordlist):
    syllab_re = re.compile(r'[^aeiouyæøå]*[aeiouyæøå]*')
    with open(wordlist) as f:
        words = [w.strip() for w in f.readlines()]
    syllabs = [list(filter(lambda x: x, syllab_re.findall(w))) for w in words]
    return list(filter(lambda x: x, syllabs))
