import random

def joined_or_empty(*args, **kwargs):
    try:
        words = joined(*args, **kwargs)
        return words
    except StopIteration:
        return ""

def number():
    return "".join([str(random.randint(1,9)) for x in range(random.randint(1,4))])

def joined(iterator, string, max_words=1, capitalize=False, numeric=False):
    words = list()
    words.append(next(iterator))

    for num in range(random.randint(1,max_words)):
        try:
            if numeric and random.getrandbits(1):
                word = number()
            else:
                word = next(iterator)

            if capitalize:
                word = word.capitalize()
            words.append(word)
        except StopIteration:
            break
    return string.join(words)

modifiers = {
    'unmodified': lambda word: next(word),
    'period': lambda word: joined(word, ". ", capitalize=True),
    'comma': lambda word: joined(word, ", "),
    'colon': lambda word: joined(word, ": "),
    'semicolon': lambda word: joined(word, "; "),
    'exclamation': lambda word: joined(word, "! ", capitalize=True),
    'question': lambda word: joined(word, "? ", capitalize=True),
    'doublequotes': lambda word: '"{}"'.format(joined(word, " ", max_words=3)) ,
    'parantesis': lambda word: '({})'.format(joined(word, " ", max_words=3)) ,
    'tuple': lambda word: '({})'.format(joined(word, ", ", max_words=3)) ,
    'brackets': lambda word: '[{}]'.format(joined(word, ", ", max_words=3)),
    'singlequotes': lambda word: "'{}'".format(joined(word, " ", max_words=3)),
    'dashed': lambda word: joined(word, "-"),
    'equals': lambda word: joined(word, " = "),
    'lessthan': lambda word: joined(word, " < "),
    'greaterthan': lambda word: joined(word, " > "),
    'slashed': lambda word: joined(word, "/"),
    'squigglies': lambda word: '{{{}}}'.format(joined(word, ", ", max_words=3)),
    'dollarsign': lambda word: "$" + next(word),
    'percent': lambda word: joined(word, " % ", numeric=True),
    'minus': lambda word: joined(word, " - ", numeric=True),
    'plus': lambda word: joined(word, " + ", numeric=True),
    'asterix': lambda word: joined(word, " * ", numeric=True),
    'division': lambda word: joined(word, " / ", numeric=True),
    'pipe': lambda word: joined(word, " | "),
    'backticks': lambda word: "`{}`".format(joined(word, " ", max_words=3)),
    'underscore': lambda word: joined(word, "_", max_words=3),
    'dots': lambda word: joined(word, ".", max_words=3),
    'camelcase': lambda word: joined(word, "", capitalize=True, numeric=False, max_words=3),
    'number': lambda word: "".join([str(random.randint(1,9)) for x in range(random.randint(1,4))]),
    'et': lambda word: "&" + next(word),
    'functioncall': lambda word: "{}({})".format(next(word), joined_or_empty(word, ", ", max_words=3))
}
